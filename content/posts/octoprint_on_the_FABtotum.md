---
title: "Octoprint on the FABtotum"
date: 2022-08-07T12:02:44+02:00
draft: true
toc: true
images:
tags:
  - 3D Printing
  - 3D Printer
  - FABtotum
  - Octoprint
---

## Introduction

Last month when I was browsing the local second-hand site I came across a listing for a 3D Printer that I had never seen.
![][fabtotum-50€]


[fabtotum-50€]: https://nvds.be/images/FABtotum_50€.png
