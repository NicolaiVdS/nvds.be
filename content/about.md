---
title: "About"
date: 2022-07-27T10:57:47+02:00
draft: false
---
![][image-1]

Hi, I am Nicolai 👋

I am a software developer but likes to tinker around with things.

### Projectsff
Planing to make a nice-looking "My Projects" page on this website to write about all cool things I worked on. Meanwhile, I am working on a [blog](/blog) to write about the things i do.

### Education
I don't have a college degree and did 2 years of applied mathematics but have been tinkering and programming since I was a kid, I find formal education to be a waste of time and resources.

---
### Website
This website is powered by [Hugo][1]. The source can be found [here][2].

[1]:	https://gohugo.io
[2]:	https://gitlab.com/NicolaiVdS/nvds.be

[image-1]:	https://nvds.be/images/me.jpg
